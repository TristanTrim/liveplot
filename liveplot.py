# MIT License
#
# Copyright (c) 2020 Mechatronics and Haptic Interfaces Lab - Rice University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# Author(s): Evan Pezent (epezent@rice.edu), Joel Linn, Tristan Trim

import mahi_gui
from mahi_gui import imgui
from mahi_gui import implot
import numpy as np
from random import random
from threading import Thread
from time import time

class PlotItem():
    data_x = np.array([])
    data_y = np.array([])
    color = imgui.Vec4()
    label = ""


colorPallet = (
imgui.Vec4(86/255,134/255,33/255,1),
imgui.Vec4(255/255,57/255,67/255,1),
imgui.Vec4(56/255,227/255,50/255,1),
imgui.Vec4(226/255,54/255,62/255,1),
imgui.Vec4(1/255,216/255,77/255,1),
imgui.Vec4(229/255,101/255,0/255,1),
imgui.Vec4(0/255,224/255,246/255,1),
imgui.Vec4(249/255,255/255,20/255,1),
imgui.Vec4(86/255,122/255,178/255,1),
imgui.Vec4(204/255,255/255,9/255,1),
imgui.Vec4(0/255,160/255,211/255,1),
imgui.Vec4(244/255,233/255,0/255,1),
imgui.Vec4(175/255,198/255,255/255,1),
imgui.Vec4(94/255,214/255,0/255,1),
imgui.Vec4(253/255,179/255,184/255,1),
imgui.Vec4(74/255,184/255,0/255,1),
imgui.Vec4(255/255,151/255,131/255,1),
imgui.Vec4(89/255,255/255,140/255,1),
imgui.Vec4(170/255,105/255,80/255,1),
imgui.Vec4(164/255,255/255,99/255,1),
imgui.Vec4(207/255,151/255,155/255,1),
imgui.Vec4(94/255,164/255,0/255,1),
imgui.Vec4(169/255,243/255,255/255,1),
imgui.Vec4(255/255,181/255,38/255,1),
imgui.Vec4(0/255,223/255,224/255,1),
imgui.Vec4(209/255,173/255,0/255,1),
imgui.Vec4(88/255,255/255,240/255,1),
imgui.Vec4(166/255,115/255,0/255,1),
imgui.Vec4(118/255,255/255,164/255,1),
imgui.Vec4(255/255,199/255,102/255,1),
imgui.Vec4(32/255,135/255,129/255,1),
imgui.Vec4(179/255,169/255,0/255,1),
imgui.Vec4(2/255,173/255,143/255,1),
imgui.Vec4(163/255,175/255,0/255,1),
imgui.Vec4(167/255,255/255,212/255,1),
imgui.Vec4(101/255,143/255,0/255,1),
imgui.Vec4(210/255,222/255,189/255,1),
imgui.Vec4(37/255,140/255,42/255,1),
imgui.Vec4(255/255,227/255,163/255,1),
imgui.Vec4(7/255,140/255,71/255,1),
imgui.Vec4(255/255,249/255,134/255,1),
imgui.Vec4(90/255,130/255,104/255,1),
imgui.Vec4(199/255,255/255,125/255,1),
imgui.Vec4(139/255,119/255,67/255,1),
imgui.Vec4(1/255,186/255,114/255,1),
imgui.Vec4(244/255,255/255,189/255,1),
imgui.Vec4(121/255,126/255,51/255,1),
imgui.Vec4(207/255,255/255,160/255,1),
)

class PlotBench(mahi_gui.Application):
    POINTS = 100
    PLOTS = 30

    def __init__(self,plotDataInput):

        self.render = imgui.Bool(True)
        self.animate  = imgui.Bool(True)
        self.followData  = imgui.Bool(True)
        self.followTime  = imgui.Bool(False)
        self.zoom = imgui.Float(10)
        self.colorIndex = 0

        self.mostRecentTime = 0

        super(PlotBench, self).__init__(800, 600, "LivePlot")
        imgui.get_io().ini_filename = None
        self.set_vsync(True)
        imgui.disable_viewports()


        self.plotDataInput = plotDataInput

        self.plotData = {}

        self.items = []

        for name in plotDataInput:
            item = PlotItem()
            item.data_x = np.empty(0, dtype="uint16")
            item.data_y = np.empty(0)
            #item.color = self.random_color()
            item.color = colorPallet[self.colorIndex]
            self.colorIndex = ( self.colorIndex+1 ) % len( colorPallet )
            item.label = name
            self.plotData[name] = item

        #self.generate_items_data()

        self.fl = open(f"bbclog{time()}.txt","w")

    def random_color(self):
        return imgui.Vec4(
                random(), 
                random(), 
                random(), 
                1)

    def generate_items_data(self):
        for i in range(len(self.items)):
            item = self.items[i]
            y = i * 0.01
            for j in range(item.data_x.shape[0]):
                item.data_x[j] = j
                item.data_y[j] = y + (random() / 50 - 0.01)
    def clear_data(self):
        for ip in self.plotDataInput:
            self.plotData[ip].data_x = np.empty(0, dtype="uint16")
            self.plotData[ip].data_y = np.empty(0)

    def slide_items_data(self):
        for i in range(len(self.items)):
            item = self.items[i]
            y = i * 0.01

            item.data_x = np.append(item.data_x,[item.data_x.shape[0]])
            #item.data_y = np.append(item.data_y,[y+(random() / 50 - 0.01)])
            item.data_y = np.append(item.data_y,[item.data_y[item.data_y.shape[0]-1]+(random() / 500 - 0.001)])

        self.POINTS += 1

           # for j in range(item.data_x.shape[0]):
           #     item.data_x[j] = j
           #     if (j+5 >= item.data_x.shape[0]):
           #         item.data_y[j] = y + (random() / 50 - 0.01)
           #     else:
           #         item.data_y[j] = item.data_y[j+5]
    def _update_data(self):
        for name,datas in self.plotDataInput.items():
            if datas:
                for data in datas:
                    if data[0]>self.mostRecentTime:
                        self.mostRecentTime = data[0]
                    self.plotData[name].data_x = np.append(
                            self.plotData[name].data_x,
                            data[0])
                    self.plotData[name].data_y = np.append(
                            self.plotData[name].data_y,
                            data[1])
                    self.fl.write(f"{name},{data[0]},{data[1]}\n")
                    self.fl.flush()
                while datas:
                    del datas[0]

    def _update(self):
        if (self.animate.value):
            #self.generate_items_data()
            #self.slide_items_data()
            self._update_data()

        width, height = self.get_window_size()
        imgui.begin(
            "Python Plot Benchmark",
            imgui.Bool(True),
            imgui.WindowFlags.NoTitleBar | imgui.WindowFlags.NoResize | imgui.WindowFlags.NoMove)
        imgui.set_window_pos(imgui.Vec2(0,0))
        imgui.set_window_size(imgui.Vec2(width, height))
        if(imgui.button("VSync On")):
            self.set_vsync(True)
        imgui.same_line()
        if(imgui.button("VSync Off")):
            self.set_vsync(False)
        imgui.same_line()
        imgui.checkbox("Render", self.render)
        imgui.same_line()
        imgui.checkbox("Animate", self.animate)
        imgui.same_line()
        imgui.checkbox("Follow Data", self.followData)
        imgui.same_line()
        imgui.checkbox("Follow Time", self.followTime)
        imgui.same_line()
        if imgui.button("clear"):
            self.clear_data()
            
        #self.zoom = imgui.drag_int("Zoom")#,self.zoom)
        #_, self.zoom = imgui.drag_int("drag int", self.zoom, min_value=10, max_value=1000)
        imgui.drag_float("Time Zoom", self.zoom, v_min=2,v_max=100)

#    (label:   str,
#     v:       mahi_gui.imgui.Int,

#     v_speed: float = 1.0,
#     v_min:   int = 0.0,
#     v_max:   int = 0.0,
#     format:  str = '%d',
#     flags:   int = SliderFlags.None_) -> bool

        imgui.text("{} lines, {} pts ea. @ {:.3f} FPS".format(self.PLOTS, self.POINTS, imgui.get_io().framerate))

        #imgui.begin_child("region", 100, -50, border=True)
        imgui.begin_child("region", imgui.Vec2(100, -50))
        for name in self.plotDataInput:
            imgui.text(name)
        imgui.end_child()
        imgui.same_line()

        #implot.set_next_plot_limits_x(0, self.POINTS,True)
        #x = self.POINTS -1 + (self.zoom.value**2 / 20)
        if self.followData.value:
            x = self.mostRecentTime + (self.zoom.value**2 / 30)
            implot.set_next_plot_limits_x(x-(self.zoom.value**2), x,True)
        elif self.followTime.value:
            x = time() + (self.zoom.value**2 / 30)
            implot.set_next_plot_limits_x(x-(self.zoom.value**2), x,True)
        else:
            x = self.mostRecentTime + (self.zoom.value**2 / 30)
            implot.set_next_plot_limits_x(x-(self.zoom.value**2), x)

        implot.set_next_plot_limits_y(-3,13)


        if (implot.begin_plot(
                "##Plot", None, None,
                imgui.Vec2(-1, -1),
                implot.Flags.NoChild,
                implot.AxisFlags.Time,
                )):
            implot.get_style().use_local_time = True
            
            if (self.render.value):
                #for item in self.items:
                for name,item in self.plotData.items():
                    implot.push_style_color(implot.Color.Line, item.color)
                    implot.plot_line(item.label, item.data_x, item.data_y)
                    implot.pop_style_color(implot.Color.Line)

            implot.push_style_color(implot.Color.Line, imgui.Vec4(1,1,1,1))
            lims = implot.get_plot_limits()
            implot.plot_line("foo", np.array((time(),time())),
                    np.array((lims.y.min,lims.y.max))
                    )
            implot.pop_style_color(implot.Color.Line)

            implot.end_plot()
        imgui.end()


class LivePlot():
    def __init__(self,names):

        self.names = names

        self.plotDataInput = {}
        for name in names:
            self.plotDataInput[name]=[]

        def thread(lp):
            lp.app = PlotBench(self.plotDataInput)
            lp.app.run()
        self.appThread = Thread(target=lambda:thread(self))
        self.appThread.start()
    def push(self,stuff):
        for thing in stuff:
            self.plotDataInput[thing]+=[stuff[thing]]
    def running(self):
        return self.appThread.is_alive()


if __name__ == "__main__":
    app = PlotBench()
    app.run()

