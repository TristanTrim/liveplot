# MIT License
#
# Copyright (c) 2020 Mechatronics and Haptic Interfaces Lab - Rice University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# Author(s): Evan Pezent (epezent@rice.edu), Joel Linn, Tristan Trim

from liveplot import LivePlot
from time import sleep, time
from random import random , sample

from threading import Thread

randomDataGenerationRate = 10 #Hz
numberOfRandomDataLines = 10

vals = {}

def continuallyUpdate(name):
    ldats = []
    while ld.running():

        vals[name] = ( time(), vals[name][1]+(random()-.5) )
        ldats.append(vals[name])

        if random() > .8:
            for ldat in ldats:
                lvals = {
                    name: ldat
                    }
                ld.push(lvals)
            ldats = []
        
        sleep(1/randomDataGenerationRate)

if __name__ == "__main__":

    dataNames = list(
            "item%d"%x for x in range(
                numberOfRandomDataLines))

    ld = LivePlot(dataNames)

    for ii, name in enumerate(dataNames):
        vals[name]=(time(),ii)

    ld.push(vals)

    for item in dataNames:
        Thread(target=lambda:continuallyUpdate(item)).start()



